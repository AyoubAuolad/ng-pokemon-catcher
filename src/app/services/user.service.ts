import { Injectable } from '@angular/core';
import { StorageKeys } from '../consts/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.utils';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user?: User;

  get user(): User | undefined {
    return this._user;
  }

  set user(user: User | undefined) {
    if (!user){
      throw new Error("What are you doing?");
    }
    StorageUtil.storageSave<User>(StorageKeys.User, user);
    this._user = user;
  }

  constructor() {

    const storedUser = sessionStorage.getItem("pokemons-user");
      if (storedUser){
        this._user = JSON.parse(storedUser);
    }

   }

   public inPokemons(id: number): boolean{
     if (this._user) {
        return Boolean(this.user?.pokemon.find((pokemon: Pokemon) => pokemon.id === id));
     }
     return false;
   }

   public addToCaught(pokemon: Pokemon): void {
     if (this._user){
       this._user.pokemon.push(pokemon);
     }
   }

   public removeFromCaught(pokemonId: number): void {
     if (this._user){
       this._user.pokemon = this._user.pokemon.filter((pokemon: Pokemon) => pokemon.id !== pokemonId);
     }
   }
}
