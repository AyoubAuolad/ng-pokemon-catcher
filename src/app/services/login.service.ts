import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, switchMap, of, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

const { apiTrainers, NG_API_KEY } = environment;

@Injectable({
  providedIn: 'root'
})

export class LoginService {


  constructor(private readonly http: HttpClient) { }

  public login( username: string ): Observable<User> {
    return this.checkUsername(username)
      .pipe(
        switchMap((user: User | undefined) => {
          if (user === undefined){
            return this.createUser(username);
          }
          return of(user);
        })
    )
  }


//Check if a user exists
private checkUsername(username:string): Observable< User|undefined >{
  return this.http.get<User[]>(`${apiTrainers}?username=${username}`)
  .pipe(
    map((response: User[]) => response.pop())
  )
}


//Create a new user
  private createUser(username: string): Observable<User>{

    const user = {
      username,
      pokemon: []
    };

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key":NG_API_KEY
    });

    return this.http.post<User>(apiTrainers, user,{
      headers
    })

  }
}
