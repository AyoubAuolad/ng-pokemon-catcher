import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, PokemonResponse } from '../models/pokemon.model';

const { apiPokemons } = environment;
const IMAGE_URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon"


@Injectable({
  providedIn: 'root'
})
export class PokemonlistService {

  private _pokemons: Pokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;

  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }

  private getImageUrl(url:string): string {
    const id = Number(url.split('/').filter( Boolean ).pop());
    return `${ IMAGE_URL }/${ id }.png`

  }

  private getPokemonId(url:string): number {
    const id = Number(url.split('/').filter( Boolean ).pop());
    return id;

  }

  public getListOfPokemons(): void {

    if (this._pokemons.length > 0){
      return;
    }

    this._loading = true;
    this.http.get<PokemonResponse>(apiPokemons)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
        .subscribe({
          //when completed
          next: ( pokemons: PokemonResponse ) => {
            this._pokemons = pokemons.results.map(pokemon => {
              return {
                ...pokemon,
                image: this.getImageUrl(pokemon.url),
                id: this.getPokemonId(pokemon.url)
              }
            });

          },
          //when something wrong happens
          error: (error: HttpErrorResponse) => {
            this._error = error.message;
          },

          complete: () => {}
        });
  }


  public pokemonById(id:number): Pokemon | undefined {
    return this._pokemons.find((pokemon: Pokemon) => pokemon.id === id)
  }
}
