import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { PokemonlistService } from './pokemonlist.service';
import { UserService } from './user.service';

const {apiTrainers, NG_API_KEY} = environment


@Injectable({
  providedIn: 'root'
})
export class CaughtPokemonService {

private _loading: boolean = false;

get loading() : boolean {
  return this._loading;
}

  constructor(
    private readonly pokemonListService: PokemonlistService,
    private readonly userService: UserService,
    private http: HttpClient
  ) { }

  public addToCaughtPokemons(id: number): Observable<User> {


    if (!this.userService.user){
      throw new Error("Catching Pokemon: There is no user");
    }
    const user: User = this.userService.user;
    const pokemon: Pokemon | undefined = this.pokemonListService.pokemonById(id);

    if(!pokemon){
      throw new Error("No pokemon with id: " + id);
    }

    if (this.userService.inPokemons(id)) {
      this.userService.removeFromCaught(id);
    } else{
      this.userService.addToCaught(pokemon)
    }



    const headers = new HttpHeaders({
      "Content-Type" : "application/json",
      "x-api-key" : NG_API_KEY
    })

    this._loading = true;



    //post the pokemon name inside the existing pokemon object
    return this.http.patch<User>(`${apiTrainers}/${user.id}`, {
      pokemon: [...user.pokemon]
    },
      { headers })
      .pipe(

        tap((updatedUser: User) => {
          this.userService.user = updatedUser;
        }),

        finalize(() => {
          this._loading = false;
        })
      )

  }
}
