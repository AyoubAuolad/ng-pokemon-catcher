import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.scss']
})
export class TrainerPage implements OnInit {

  get user(): User | undefined {
    return this.userService.user;
  }

  get caught(): Pokemon[] {
    if (this.userService.user) {
      return this.userService.user.pokemon;
    }

    return [];
  }
  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

}
