import { Component, OnInit } from '@angular/core';
import { Pokemon, PokemonResponse } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { PokemonlistService } from 'src/app/services/pokemonlist.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-pokemoncatalogue',
  templateUrl: './pokemoncatalogue.page.html',
  styleUrls: ['./pokemoncatalogue.page.scss']
})
export class PokemoncataloguePage implements OnInit {

  get pokemons(): Pokemon[] {
    return this.pokemonListService.pokemons;
  }

  get loading(): boolean {
    return this.pokemonListService.loading;
  }

  get error(): string {
    return this.pokemonListService.error;
  }

  get user(): User | undefined {
    return this.userService.user;
  }

  constructor(
    private userService: UserService,
    private readonly pokemonListService: PokemonlistService ) { }

  ngOnInit(): void {
    this.pokemonListService.getListOfPokemons();
  }


}
