import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemoncataloguePage } from './pokemoncatalogue.page';

describe('PokemoncataloguePage', () => {
  let component: PokemoncataloguePage;
  let fixture: ComponentFixture<PokemoncataloguePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemoncataloguePage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemoncataloguePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
