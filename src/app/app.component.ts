import { Component, OnInit } from '@angular/core';
import { PokemonlistService } from './services/pokemonlist.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'pokemon-catcher-app';

  /**
   *
   */
  constructor( private readonly userService: UserService,
    private readonly pokemonListService: PokemonlistService) {


  }

  ngOnInit(): void {
    if (this.userService.user){
      this.pokemonListService.getListOfPokemons();
    }
  }
}
