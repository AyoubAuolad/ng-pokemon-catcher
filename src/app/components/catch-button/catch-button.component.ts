import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { CaughtPokemonService } from 'src/app/services/caught-pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.scss']
})
export class CatchButtonComponent implements OnInit {

  public isCaught: boolean = false;


  @Input() pokemonId: number = 0;

  get loading() : boolean {
    return this.caughtPokemonsService.loading;
  }

  constructor(private readonly caughtPokemonsService: CaughtPokemonService, private readonly userService: UserService) { }

  ngOnInit(): void {
    this.isCaught = this.userService.inPokemons(this.pokemonId);
  }

  onCatchClick(): void {
    this.caughtPokemonsService.addToCaughtPokemons(this.pokemonId)
    .subscribe({
      next: (response: User) => { this.isCaught = this.userService.inPokemons(this.pokemonId);
      },
      error: (error: HttpErrorResponse) => { console.log("ERROR: ", error.message); }
    })



  }

}
