import { Component, Input, OnInit, Output } from '@angular/core';
import { Pokemon, PokemonResponse } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.scss']
})
export class PokemonListItemComponent implements OnInit {

  @Input() pokemon?: Pokemon;

  constructor() { }

  ngOnInit(): void {
  }

}
