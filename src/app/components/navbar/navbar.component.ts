import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Input() pokemons: Pokemon[] = [];

  get user(): User | undefined{
    return this.userService.user;
  }

  constructor(
    private readonly userService: UserService
  ) { }

  ngOnInit(): void {
  }

}
