import { Component, OnInit, Input } from '@angular/core';
import { Pokemon, PokemonResponse } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.scss']
})
export class PokemonsComponent implements OnInit {

  @Input() pokemons: Pokemon[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
