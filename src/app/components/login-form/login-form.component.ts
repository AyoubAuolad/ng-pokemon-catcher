import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  //Emits event to parent component
  @Output() login: EventEmitter<void> = new EventEmitter()

  public loading: boolean = false;
  public error: string = "";

  constructor(private readonly loginService: LoginService,
    private userService: UserService) { }

  ngOnInit(): void {
  }

  //Get the matched user
  public onLoginSubmit(form: NgForm){
    this.loading = true;

    console.log(form.value);
    const { username } = form.value;
    this.loginService.login(username).subscribe({

      next: (user: User | undefined) => {
        if (user === undefined){
          this.error = "No such user";
        } else {
          this.userService.user = user;
          this.login.emit();
          ;
        }
      },

      error: (error: HttpErrorResponse) => {
        this.error = error.message;
      },

      complete: () => {
        this.loading = false;
      }
    })
  }

}
