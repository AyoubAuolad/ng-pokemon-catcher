import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginPage } from './pages/login/login.page';
import { PokemoncataloguePage } from './pages/pokemoncatalogue/pokemoncatalogue.page';
import { TrainerPage } from './pages/trainer/trainer.page';

const routes: Routes = [
  {
    path:"",
    pathMatch:"full",
    redirectTo:"/login"
  },
  {
    path:"login",
    component:LoginPage
  },
  {
    path:"pokemoncatalogue",
    component:PokemoncataloguePage,
    canActivate:[ AuthGuard ]
  },
  {
    path:"trainer",
    component:TrainerPage,
    canActivate: [ AuthGuard ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
